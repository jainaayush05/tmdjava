package com.truemd.rest;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.jersey.core.header.FormDataContentDisposition;

import eSignASPLib.AppearanceStyle;
import eSignASPLib.AuthMetaDetails;
import eSignASPLib.AuthModeType;
import eSignASPLib.Response;
import eSignASPLib.Settings;
import eSignASPLib.SignatureAppearance;
import eSignASPLib.eSign;
 
@Path("/hello")
public class DigitalSignService {
 
	
	private static String BASE_PATH= new DigitalSignService().getClass().getResource("/").getFile().replaceFirst("/", "");
	private static String UPLOAD_DIR=System.getProperty("java.io.tmpdir")+File.separator;//"H:\\workspace\\trueSignTest1\\Upload\\";
	static {
		Settings.AspId = "PRETRUE";
        Settings.AspSigngingPfxFilePath = BASE_PATH+ "\\Docsigntest.pfx";///Users/visheshagarwal/Desktop/AndroidCode/TestappTrueSign1/lib
        Settings.AspSigngingPfxPassword = "emudhra";
        Settings.AspSigngingPfxAlias = "le-65a36686-6718-4f6f-89be-a9e87d9186d5";
        Settings.UidaiEncryptionCerFilePath = BASE_PATH+ "\\PreProd.cer";
        Settings.OtpURL = "https://server2.e-mudhra.com:8443/eServices/v1_6/getotp";//"http://espserver.com:8484/eServices/v1_6/getotp";//
        Settings.eSignURL = "https://server2.e-mudhra.com:8443/eServices/v1_6/signdoc";//http:// espserver.com:8484/eServices/v1_6/signdoc";
        Settings.AuthModeType = AuthModeType.OTP;
	}
	public static void main(String[] args) {
		URL r = new DigitalSignService().getClass().getResource("/");	
		System.out.println(r.getFile().replaceFirst("/", ""));
		//getClasspath();
	}
	@GET
	@Path("classpath")
	public static String getClasspath(){
		System.out.println(System.getProperties());
		//System.out.println(absolutePath +" "+file.getParentFile().getAbsolutePath());//ss
		return BASE_PATH +" "+ UPLOAD_DIR ;
	}
	
	@GET
	@Path("requestOTP/{aadhar}")
	public String requestOTP(@PathParam("aadhar")String aadharNumber){
       eSign objeSign = new eSign();
       String transactionid= System.currentTimeMillis()+"";
       Response resp = objeSign.GetOTP(aadharNumber, transactionid);
       return resp.getStatus().toString() +" - "+transactionid;
	}

    @POST
    @Path("/upload/pdf")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces("application/pdf")
    public javax.ws.rs.core.Response uploadAndSignPdf(
            @FormDataParam("file") InputStream fileInputStream,@FormDataParam("file") FormDataContentDisposition fileFormDataContentDisposition,
            @FormDataParam("adharNumber") String adhar,@FormDataParam("transactionId") String transactionId,@FormDataParam("otp") String otp) {
    	System.out.println("Got !! Adhar "+ adhar+" transaction "+transactionId+" otp "+otp);
        // local variables
        String fileName = null;
        String uploadFilePath = null;
 
        try {
            fileName = fileFormDataContentDisposition.getFileName();
            uploadFilePath = writeToFileServer(fileInputStream, fileName);
	         String targetpdf= uploadFilePath.replace(".pdf", "-signed.pdf");
            Response resp = signPdf(adhar,transactionId,otp,uploadFilePath,targetpdf);
            // TODO response check and exception handeling
            File file = new File(targetpdf);
    		ResponseBuilder response = javax.ws.rs.core.Response.ok((Object) file);
    		response.header("Content-Disposition",
    				"attachment; filename="+fileName);
    		return response.build();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally{
            // release resources, if any
        }
        
       return javax.ws.rs.core.Response.serverError().build();

    }

    
	private Response signPdf(String aadharNumber,String transactionId,String otp, String pdfToSign, String targetpdf  ){
			//Assigning the values to SignatureAppearance class
	        SignatureAppearance objAppearance = new SignatureAppearance();

	        objAppearance.SignedBy = "firstname middlename lastname";
	        objAppearance.Location = "Bangalore";
	        objAppearance.Reason = "eSign Test";
	        objAppearance.PositionFromLeft = 20;
	        objAppearance.PositionFromBottom = 100;
	        objAppearance.Style = AppearanceStyle.PenLogo_With_Name_And_Description;

	         //Assigning the values to AuthMetaDetails
	         AuthMetaDetails objAuthMetaDetails = new AuthMetaDetails();
	         objAuthMetaDetails.fdc = "NA";
	         objAuthMetaDetails.idc = "NA";
	         objAuthMetaDetails.lot = "P";
	         objAuthMetaDetails.lov = "452010";
	         objAuthMetaDetails.pip = "NA";
	         objAuthMetaDetails.udc = "eSignASP25082016V123";
	         // Use this method to trigger OTP to Aadhaar Holder Mobile / Email ID
	        eSign objeSign = new eSign();
	        Response resp = objeSign.Sign(aadharNumber, otp, transactionId, pdfToSign, 
	        		targetpdf, objAuthMetaDetails, objAppearance);
	        System.out.println("\nerror:"+ resp.getErrorCode()+ resp.getErrorMessage() +"\nstatus:"+ resp.getStatus()+"\nresponseXml:"+ resp.getResponseXML()+"\nrequest:"+ resp.getRequestXML());
	        return resp ;
	        		//"\nerror:"+ resp.getErrorCode()+ resp.getErrorMessage() +"\nstatus:"+ resp.getStatus()+"\nresponseXml:"+ resp.getResponseXML()+"\nrequest:"+ resp.getRequestXML();
		        
	}
	
	 // http://localhost:8080/Jersey-UP-DOWN-PDF-File/rest/fileservice/upload/pdf
   
    /**
     * write input stream to file server
     * @param inputStream
     * @param fileName
     * @throws IOException
     */
    private String writeToFileServer(InputStream inputStream, String fileName) throws IOException {
 
       OutputStream outputStream = null;
        String qualifiedUploadFilePath = UPLOAD_DIR + fileName;
 
        try {
            outputStream = new FileOutputStream(new File(qualifiedUploadFilePath));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            outputStream.flush();
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        finally{
            //release resource, if any
            outputStream.close();
        }
        return qualifiedUploadFilePath;
    }
}
	
